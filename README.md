# kirech // _Rechnen fuer Kinder_

This repo contains scripts for training of arithmetics.

I started it for my own kids; might be useful for other people.

Documentation might be lacking, because the scripts
initially were only meant to be used by myself.

Use/modify/distribute as you like:
**PUBLIC DOMAIN, without guarantee for anything, use at your own risk!**

---

## kire.sh

This shell script provides training for
addition, subtraction, multiplication, division, and square roots
of integer numbers.

It recognizes the following commands, either given (as single argument)
on the command line, or during the command loop by direct input:

- q: quit script
- h[elp]: one line displaying the available commands
- res[ult]: display the result of the current problem
- add[ition]/sub[traction]/mul[tiplication]/div[ision]/sq/sqrt: select mode

`sq` (squares) is a special case of multiplication, where the two
factors are identical.
`sqrt` (square root) asks for the square root of a single integer
chosen in such a way that the result will be an integer as well.

All mode selection provides input of lower (`<--`) and upper (`-->`)
limits. In case of empty input, standard or last values are used.

Then a new problem is generated and displayed, and the answer can be given.
If it is correct, a new problem is generated, otherwise `!?` is displayed,
and another try for the same problem can be entered. By entering `res` the
correct answer can be displayed, and a new problem is generated immediately.

The problem mode can be changed at any time, by entering the corresponding
command (add/sub/mul/div/sq/sqrt).

---

_(2017-Mar, YB)_
