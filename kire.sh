#!/bin/sh
# calculation training (2017-Mar-27 YB)
# default summation limits
los=0
his=50
# default factor limits
lof=1
hif=10

# input sanitizer: only numbers, dot, minus, lowercase
sanit() { echo $1|tr 'A-Z' 'a-z'|tr -c -d '0-9a-z.-' ; }

# get low and high limits, using arguments for default values
setlim() {
 cat <<EOH
 $1
<-- ? [$2]
EOH
 read s
# set default low limit, if empty
 s=${s:-$2}
 lov=`sanit $s`
 echo '--> ?' [$3]
 read s
# set default low limit, if empty
 s=${s:-$3}
 hiv=`sanit $s`
 echo $1: $lov ... $hiv
}

# random number mod $1
# get 8 bytes from urandom, convert to decimal, add and 'mod $1' using dc
rand() {
dd bs=8 if=/dev/urandom count=1 2>/dev/null | od -t u2 | head -n 1 | sed -e "s/ 	*[^ 	][^ 	]*[ 	]*//;s/  */+/g;s/^/0 /;s/$/+/;s/$/0k$1%p/" | dc 2>/dev/null
}

# random value between $1 and $2
randwin() { echo $(( $1+`rand $((1+$2-$1))` )) ; }

# initial command from arg.1, or help
com=`sanit $1`
com=${com:-h}
# result
res=q
# problem mode
mod=none

# main loop until quit
while test $com != q
do
# flag for keeping problem, or generation of new one
prob=new

# process command
case $com in
h*) echo ::: $0 h/q/res/add/sub/mul/div/sq/sqrt :::
 prob=new
 ;; # help
$res) ;; # correct answer given
add*) setlim Add $los $his
 los=$lov
 his=$hiv
 mod=add
 ;;
sub*) setlim Sub $los $his
 los=$lov
 his=$hiv
 mod=sub
 ;;
mul*) setlim Mult $lof $hif
 lof=$lov
 hif=$hiv
 mod=mul
 ;;
div*) setlim Divi $lof $hif
 lof=$lov
 hif=$hiv
 mod=div
 ;;
sq) setlim Square $lof $hif
 lof=$lov
 hif=$hiv
 mod=sq
 ;;
sqrt) setlim Sq.root $lof $hif
 lof=$lov
 hif=$hiv
 mod=sqrt
 ;;
# show result
res*) echo : = $res
 ;;
# in case of wrong answer, try again
*) echo '!?'
 prob=keep
 ;;
esac # process command

# make sure hif>lof, to prevent endless loop in problem generation
if test $lof -gt $hif
# swap if necessary
then tv=$hif
 hif=$lof
 lof=$tv
fi

# make sure his!=los
if test $los = $his
then his=$(($los+10))
fi

if test $prob = new
# generate new problem, always according to
# principle "value1 operator value2 = result"
then case $mod in
add)
 v1=`randwin $los $(($his-1))`
 res=`randwin $v1 $his`
 v2=$(($res-$v1))
 op='+'
 ;;
sub)
 v1=`randwin $((1+$los)) $his`
 res=`randwin $los $v1`
 v2=$(($v1-$res))
 op='-'
 ;;
mul)
 v1=`randwin $lof $hif`
 v2=`randwin $lof $hif`
 res=$(($v1*$v2))
 op='*'
 ;;
div)
# force first iteration
 v1=$(($hif+1))
# iterate until all values are between limits
 while test $v1 -gt $hif -o $v1 -lt $lof
 do
  v2=`randwin $lof $(( $lof+($hif-$lof)/2 ))`
  if test $v2 = 0
  then v2=1
  fi
  res=`randwin $lof $hif`
  v1=$(($res*$v2))
 done
 op='/'
 ;;
sq)
 v1=`randwin $lof $hif`
 v2=$v1
 res=$(($v1*$v2))
 op='*'
 ;;
sqrt)
 v2=`randwin $lof $hif`
 v1=$(($v2*$v2))
 res=$v2
 ;;
none) ;; # do nothing here, in case of help shown
*) echo internal error: unknown problem type $mod
 mod=none ;;
esac # new problem
fi

# show problem, if mode is known
case $mod in
none) ;; # do nothing here
sqrt) echo : sqrt $v1 = ;;
*) echo : $v1 "$op" $v2 = ;; # assume infix and two operands as normal case
esac

# get and sanitize answer or command
read s _
com=`sanit $s`
# show help, if empty command
com=${com:-h}
done
